# imu

#### 项目介绍
适用于维特智能WT61C（六轴惯导）的ROS包


#### 使用说明

1. rosrun imu imu.py _port:=<对应的USB串口，例如：/dev/imu>
2. imu节点发布/imu（sensor_msgs/Imu）
3. https://www.cnblogs.com/Ezekiel/p/9913119.html


